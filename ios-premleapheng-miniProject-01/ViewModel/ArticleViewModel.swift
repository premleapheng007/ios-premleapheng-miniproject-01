//
//  ArticleViewModel.swift
//  ios-premleapheng-miniProject-01
//
//  Created by BTB_015 on 12/15/20.
//

import Foundation
import UIKit


struct ArticleViewModel {
    
    static let shared = ArticleViewModel()
    
    func fetchData(completion: @escaping (([ArticleModel]),Error?) -> ()) {
        ArticleService.shared.fetchData { (article, error) in
            if error != nil {
                completion([],error)
                return
            }
            
            var articleModels = [ArticleModel]()
            articleModels = article.compactMap(ArticleModel.init)
            
            completion(articleModels,nil)
            
        }
    }
    
    func deleteData(id: String, completion: @escaping(String,Error?)->()) {
        ArticleService.shared.deleteData(id: id) { (message, error) in
            if error != nil {
                completion(message,error)
                return
            }
            completion(message,nil)
        }
    }
    
//    func postMethod(title: String, description: String,image: String, completion: @escaping (String,Error?)->()){
//        ArticleService.shared.postMethod(title: title, description: description, image: image) { (message, error) in
//            if error != nil {
//                completion(message,error)
//                return
//            }
//            completion(message,nil)
//        }
//    }
    
    func postData(image: UIImage,title: String, description: String, completionHandler: @escaping (_ imageResponse: ImageResponsePost) -> Void) {
        ArticleService.shared.uploadImage(image: image, completionHandler: { (imageResponse) in
            DispatchQueue.main.async {
                
//                completionHandler(imageResponse)
                
                ArticleService.shared.postMethod(title: title, description: description, image: imageResponse.url) { (message, error) in
                    if error != nil {
                        print("post Error")
                        return
                    }
                    print("post successfully")
                }
            }
        })
    }
    
    func updateArticle(articleId: String, article: ArticleRequestPost, completion: @escaping (_ messageResponse: MessageResponsePost) -> Void ) {
        ArticleService.shared.updateData(articleId: articleId, article: article) { (messageResponse) in
            DispatchQueue.main.async {
                completion(messageResponse)
            }
        }
    }

}
