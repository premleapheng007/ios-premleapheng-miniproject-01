//
//  DetailViewController.swift
//  ios-premleapheng-miniProject-01
//
//  Created by BTB_015 on 12/15/20.
//

import UIKit
import Kingfisher

class DetailViewController: UIViewController,SendArticleDelegate {

    

    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var ariticleDesc: UILabel!
    @IBOutlet weak var articleProfile: UIImageView!
    @IBOutlet weak var articleName: UILabel!
    @IBOutlet weak var articleDate: UILabel!
    @IBOutlet weak var detailDesc: UITextView!
    
    var arcID: String = ""
    var arcImage: String = ""
    var desc: String = ""
    var profile: String = ""
    var arcName: String = ""
    var arcDate: String = ""
    var detailDes: String = ""
    
    var delegateObj: SendArticleDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        ariticleDesc.text = desc
        articleProfile.image = UIImage(named: profile)
        articleName.text = arcName
        articleDate.text = arcDate
        detailDesc.text = detailDes
        articleImage.kf.setImage(with: URL(string: arcImage) , placeholder: UIImage(named: "other"), options: nil, progressBlock: nil) { [self] result in
                     switch result {
                     case .success( _):
                         break
                     case .failure(let error):
                         if error.isInvalidResponseStatusCode {
                             articleImage.image = UIImage(named: "other")
                     }
                 }
              }
         
    }
    
    func sennArticleData(id: String,artitcleImg: String, aritcleTitle: String, articleDesc: String, articleProfile: String, aritcleName: String, articleDate: String, articleCategory: String) {
         arcID = id
         arcImage = artitcleImg
         desc  = aritcleTitle
         profile = articleProfile
         arcName = aritcleName
         arcDate = articleDate
         detailDes = articleDesc
    }

    @IBAction func editArticle(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "detailVC") as! UpdateViewController
        delegateObj = vc
        delegateObj?.sennArticleData(id: arcID, artitcleImg: arcImage, aritcleTitle: desc, articleDesc: detailDes, articleProfile: profile, aritcleName: arcName, articleDate: arcDate, articleCategory: "General")
        
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func deleteArticle(_ sender: Any) {

        let alert = UIAlertController(title: "UIAlertController", message: "Would you like to continue learning how to use iOS alerts?", preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { action in
            ArticleViewModel.shared.deleteData(id: self.arcID) { (message, error) in
                print(message)
            }
//            self.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)

        }))
        
        alert.addAction(UIAlertAction(title: "Cancle", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)

    }
}
