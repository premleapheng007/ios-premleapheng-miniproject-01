//
//  ViewController.swift
//  ios-premleapheng-miniProject-01
//
//  Created by BTB_015 on 12/9/20.
//

import UIKit
import Kingfisher

protocol SendArticleDelegate {
    func sennArticleData(id: String, artitcleImg: String, aritcleTitle: String, articleDesc: String, articleProfile: String, aritcleName: String, articleDate: String, articleCategory: String)
}

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var articles = [ArticleModel]()
    let network = ArticleViewModel.shared
    
    var delegateObj: SendArticleDelegate?
    
    var profile = ["other","Profile"]
    var userName = ["lulu","lili"]
    
    override func viewDidLoad() {
    
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        
        tableView.register(UINib(nibName: "ArticleItemTableViewCell", bundle: nil), forCellReuseIdentifier: "itemCell")
        tableView.showsHorizontalScrollIndicator = false
        tableView.separatorStyle = .none
        
        fetchData()
        
        //Refresh Data
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(didPullRefresh), for: .valueChanged)

    }
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    @objc func didPullRefresh() {
            fetchData()
            DispatchQueue.main.async {
                self.tableView.refreshControl?.endRefreshing()
            }
        }

    func fetchData(){
        network.fetchData { (articles, error) in
            if error != nil{
                return
            }
            
            self.articles = articles
            self.tableView.reloadData()
            
        }
    }

}
extension ViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "detailVC") as! DetailViewController
        delegateObj = vc
        delegateObj?.sennArticleData(id: articles[indexPath.row].id,artitcleImg: articles[indexPath.row].image , aritcleTitle: articles[indexPath.row].title, articleDesc: articles[indexPath.row].description, articleProfile: profile.randomElement()! , aritcleName: userName.randomElement()! , articleDate: articles[indexPath.row].createdAt, articleCategory: articles[indexPath.row].category?.name ?? "General")
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath) as! ArticleItemTableViewCell
        
        cell.articleDescription.text = articles[indexPath.row].title
        cell.articleImage.kf.setImage(with: URL(string: articles[indexPath.row].image) , placeholder: UIImage(named: "other"), options: nil, progressBlock: nil) { result in
                    switch result {
                    case .success( _):
                        break
                    case .failure(let error):
                        if error.isInvalidResponseStatusCode {
                            cell.articleImage.image = UIImage(named: "other")
                    }
                }
             }
        cell.createdDate.text = articles[indexPath.row].createdAt
        cell.category.text = articles[indexPath.row].category?.name ?? "General"
        cell.userName.text = userName.randomElement()
        return cell
    }
    
    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
            let shareAction = UIAction(title: "Share", image: UIImage(systemName: "square.and.arrow.up")) { _ in
                print("Share")
                }
            let edit = UIAction(title: "Edit", image: UIImage(systemName: "pencil.circle.fill")) { _ in
//                  self.sentDataForUpate(indexPath: indexPath.row)
                }
            let delete = UIAction(title: "Delete", image: UIImage(systemName: "trash.circle.fill")) { _ in
                let alert = UIAlertController(title: "Delete Item", message: "Do you want to delete?", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { _ in
                        self.network.deleteData(id: self.articles[indexPath.row].id) { (message, error) in
                            print("delete successfully")
                            
                            self.tableView.reloadData()
                            self.fetchData()
                            
                        }
                    }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
        
                // Pass the indexPath as the identifier for the menu configuration
            return UIContextMenuConfiguration(identifier: indexPath as NSIndexPath, previewProvider: nil) { _ in
            return UIMenu(title: "", children: [edit, delete, shareAction])
        }
    }
}


