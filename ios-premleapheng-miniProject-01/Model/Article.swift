//
//  Article.swift
//  ios-premleapheng-miniProject-01
//
//  Created by JONNY on 9/23/1399 AP.
//

import Foundation

// MARK: - Ariticle
struct Article: Codable {
    let id: String
    let title: String
    let description: String
    let image: String?
    let createdAt: String
    let updatedAt: String
    var category: Category?
    var author: Author?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case title
        case description
        case image
        case createdAt
        case updatedAt

    }
}
