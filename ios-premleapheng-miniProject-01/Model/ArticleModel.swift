//
//  ArticleModel.swift
//  ios-premleapheng-miniProject-01
//
//  Created by BTB_015 on 12/15/20.
//

import Foundation

struct ArticleModel {
    var id: String
    var title: String
    var description: String
    var image: String
    var createdAt: String = ""
    var updatedAt: String = ""
    var category: Category?
    var author: Author?
    
    init(article: Article) {
        self.id = article.id
        self.title = article.title
        self.description = article.description
        self.image = article.image ?? "no-image"
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        if let cdate = dateFormat.date(from: article.createdAt) {
            let newDate = DateFormatter()
            newDate.dateFormat = "dd-MMM-yyyy"
            self.createdAt = "Date: \(newDate.string(from: cdate))"
        }

        if let udate = dateFormat.date(from: article.updatedAt) {
            let newDate = DateFormatter()
            newDate.dateFormat = "dd-MMM-yyyy"
            self.updatedAt = "Date: \(newDate.string(from: udate))"
        }
    }
}
