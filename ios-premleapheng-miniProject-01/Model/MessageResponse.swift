//
//  MessageResponse.swift
//  ios-premleapheng-miniProject-01
//
//  Created by BTB_015 on 12/19/20.
//

import Foundation

struct MessageResponsePost: Codable {
    let data: DataResponsePost
    let message: String
}
struct DataResponsePost: Codable {
    let id, title, description: String
    let published: Bool
    let image: String
    let createdAt, updatedAt: String

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case title, description, published, image, createdAt, updatedAt
    }
}
