//
//  Category.swift
//  ios-premleapheng-miniProject-01
//
//  Created by BTB_015 on 12/15/20.
//

import Foundation

struct Category: Codable {
    var name: String
}
