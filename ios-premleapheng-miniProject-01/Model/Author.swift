//
//  Author.swift
//  ios-premleapheng-miniProject-01
//
//  Created by BTB_015 on 12/15/20.
//

import Foundation

struct Author: Codable {
    var id: String
    var name: String
    var image: String
}
