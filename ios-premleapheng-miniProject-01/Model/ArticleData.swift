//
//  ArticleData.swift
//  ios-premleapheng-miniProject-01
//
//  Created by JONNY on 9/23/1399 AP.
//

import Foundation

// MARK: - Data
struct ArticleData: Codable {
    let data: [Article]
}
