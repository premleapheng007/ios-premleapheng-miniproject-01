//
//  ArticlePostRequest.swift
//  ios-premleapheng-miniProject-01
//
//  Created by BTB_015 on 12/19/20.
//

import Foundation

struct ArticleRequestPost: Codable{
    
    let title: String
    let description: String
    let published: Bool
    let image: String
}
