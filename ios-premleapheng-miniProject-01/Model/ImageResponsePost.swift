//
//  ArticleResponse.swift
//  ios-premleapheng-miniProject-01
//
//  Created by BTB_015 on 12/18/20.
//

import Foundation
 
struct ImageResponsePost: Codable {
    let id: String
    let url: String
    let createdAt, updatedAt: String

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case url, createdAt, updatedAt
    }
}

