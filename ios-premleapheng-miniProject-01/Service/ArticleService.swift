//
//  ArticleService.swift
//  ios-premleapheng-miniProject-01
//
//  Created by BTB_015 on 12/15/20.
//

import Foundation
import Alamofire

struct ArticleService {
    
    // Scelarton
    static let shared = ArticleService()
    
    //mainURL
    let mainURL = "http://110.74.194.124:3000/api"
    let uploadImageURL = "http://110.74.194.124:3000/api/images"
    
    func fetchData(completion:@escaping (([Article]),Error?) -> ()) {
        
        //Check URL  have data or not
        guard let url = URL(string: "\(mainURL)/articles") else{
            return
        }
 
        URLSession.shared.dataTask(with: url) { (data , response, error) in
            
            //Check data with error
            
            if data == nil && error != nil {
                completion([],error)
                return
            }
            
            //mapping data
            do{
                if let safeData  = data {
                    let articleData = try JSONDecoder().decode(ArticleData.self, from: safeData)
                    
                    let article = articleData.data

                        DispatchQueue.main.sync {
                            completion(article,nil)
                        }
                }
            }catch let error{
                completion([],error)
                print(error)
            }
        }.resume()
    }
    
    
    
    func deleteData(id: String,completion: @escaping (String,Error?)->()){
       
        guard let url = URL(string: "\(mainURL)/articles/\(id)") else {
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                completion("Delete Data not successfully",error)
                return
            }
            DispatchQueue.main.sync {
                completion("Delete Data Successfully",nil)
            }
        }.resume()
    }
    
    
    
    func postMethod(title: String, description: String,image: String, completion: @escaping (String,Error?)->()) {
        
        guard let url = URL(string: "\(mainURL)/articles") else {
            return
        }
        
        // Create model
        struct UploadData: Codable {
            let title: String
            let description: String
            let image: String
        }
        
        // Add data to the model
        let uploadDataModel = UploadData(title: title, description: description, image: image)
        
        // Convert model to JSON data
        guard let jsonData = try? JSONEncoder().encode(uploadDataModel) else {
            print("Error: Trying to convert model to JSON data")
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        request.httpBody = jsonData
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {return}
            guard let data = data else {return}

            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: data) as? [String: Any] else {return}
                guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {return}
                guard let prettyPrintedJson = String(data: prettyJsonData, encoding: .utf8) else {return}
                completion(prettyPrintedJson, error)
                print(prettyPrintedJson)
            } catch {
                print("Error: Trying to convert JSON data to string")
                return
            }
        }.resume()
    }
    
    func uploadImage(image: UIImage, completionHandler: @escaping(_ imageResponse: ImageResponsePost) -> Void) {
            guard let url = URL(string: "http://110.74.194.124:3000/api/images") else {
                return
            }
            
            let boundary = UUID().uuidString
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = "POST"
            urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

            var data = Data()
            data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
            data.append("Content-Disposition: form-data; name=\"image\"; filename=\".png\"\r\n".data(using: .utf8)!)
            data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
            data.append(image.pngData()!)
            data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)

        URLSession.shared.uploadTask(with: urlRequest, from: data, completionHandler: { responseData, response, error in
            if error == nil {
                do {
                    let imageResponse = try JSONDecoder().decode(ImageResponsePost.self, from: responseData!)
                    DispatchQueue.main.sync {
                        completionHandler(imageResponse)
                    }
                } catch let err {
                    print("ERROR: ",err)
                }
            }
        }).resume()
    }
    
    func updateData(articleId: String, article: ArticleRequestPost, completion: @escaping (_ messageResponse: MessageResponsePost) -> Void) {
        
            guard let url = URL(string: "\(mainURL)/\(articleId)") else {
                return
            }
            guard let jsonData = try? JSONEncoder().encode(article) else {
                return
            }
            
            var request = URLRequest(url: url)
            request.httpMethod = "PATCH"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.httpBody = jsonData
            
            URLSession.shared.dataTask(with: request) { data, response, error in
                guard error == nil else {
                    return
                }
                guard let data = data else {
                    return
                }
                do {
                    let messageReponse = try JSONDecoder().decode(MessageResponsePost.self, from: data)
                    DispatchQueue.main.sync {
                        completion(messageReponse)
                    }
                } catch let err {
                    print("ERROR: \(err.localizedDescription)")
                }
            }.resume()
            
        }
}

